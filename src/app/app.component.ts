import { Component } from '@angular/core';

import { AWSService } from './app.service';

@Component({
  selector: 'app-root',
  providers: [AWSService],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor(private awsService: AWSService) {
    awsService.listBuckets();
    // awsService.upload();
    // awsService.uploadFile();
    awsService.download();
  }
}


