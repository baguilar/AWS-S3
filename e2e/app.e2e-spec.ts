import { AWSS3Page } from './app.po';

describe('aws-s3 App', () => {
  let page: AWSS3Page;

  beforeEach(() => {
    page = new AWSS3Page();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
