# Upload files using Amazon Simple Storage Service (S3)

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.1.0.

## S3 regions

See [regions](http://docs.aws.amazon.com/general/latest/gr/rande.html#s3_region)

## Running with Chromium

It does not support `localhost` or `127.0.0.1` for CORS requests ([see this](https://stackoverflow.com/a/28569497)) so it is necessary to load Chromium with a special setting

```shell
$ chromium  --disable-web-security --user-data-dir
```

## Setup project

### Install dependencies

The packages used for setup the project are:

- [Angular-CLI](https://github.com/angular/angular-cli)


```shell
$ sudo npm -g angular-cli
```

### Generate the project

```shell
$ ng new AWS-S3
```
### Extract webpack

```shell
$ ng eject
```

## Run the project

```shell
$ npm start
```

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
